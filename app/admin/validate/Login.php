<?php
namespace app\admin\validate;
use think\Validate;

class Login extends Validate
{
    protected  $rule = [
                            'admin_name'    => 'require',
                            'admin_pwd'     => 'require',
                            //'vercode'       =>'require|captcha'
                ];

    protected $message = [
                'admin_name.require'    => '账户不能为空！！！',
                'admin_pwd.require'     => '密码不能为空！！！',
                'vercode.require'       => '验证码不能为空！！！',
                //'vercode.captcha'       => '验证码不正确！！！'
        ];
}