<?php
/*
 * @Descripttion: 管理员模型
 * @version: 
 * @Author: cmg
 * @Date: 2020-01-08 15:37:12
 * @LastEditTime : 2020-01-09 10:56:10
 */


namespace app\admin\model;
use think\Model;
class AdminUser extends AdminModel
{
    protected $autoWriteTimestamp = true;
    protected $createTime  = 'create_time';
    protected $updateTime  = 'update_time';
}