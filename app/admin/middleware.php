<?php
/*
 * @Descripttion: 
 * @version: 
 * @Author: cmg
 * @Date: 2020-01-09 11:02:50
 * @LastEditTime : 2020-01-09 11:03:05
 */



return [
     \think\middleware\LoadLangPack::class,

     \think\middleware\SessionInit::class,
    //日志
    \app\admin\middleware\AdminLog::class,
];

