<?php
/*
 * @Descripttion: 
 * @version: 
 * @Author: cmg
 * @Date: 2020-01-07 10:45:50
 * @LastEditTime : 2020-01-09 11:44:17
 */
namespace app\admin\controller;
//模型
use app\admin\model\AdminUser;
use app\admin\model\AuthRule;
use app\admin\model\AuthGroup;
use app\admin\model\AuthGroupAccess;
//模型
//验证
use app\admin\validate\Login as login_validate;
//验证
use think\exception\ValidateException;
use think\facade\Session;
use app\common\controller\AdminBase;
//
use cmg\helper\SignHelper;

class Login extends AdminBase{
	
	/*
     * 登录
     */
    public function initialize()
    {
        parent::initialize(); 
    }
	public function index(){

	    /*处理登录*/
	    if(request()->isPost()){
	        $data=request()->Post();
            try {
                validate(login_validate::class)->check($data);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return toError(202,$e->getError());
			}
            $res = self::checkLogin($data);
            if($res){
                return toSuccess("登录成功",["url"=>"/admin/index/index"]);
            }else{
                return toError($res[0],$res[1]);
            }
        }
        //检测是否登录过
            $admin= Session::get('admin_info');
            $admin_sign= Session::get('admin_sign') == SignHelper::authSign($admin) ? $admin['id'] : 0;
            
            // 签名验证
            if ($admin && $admin_sign) {
                return  redirect('/admin/index/index');
            }
        /*登录界面渲染*/
	    return View();
	}


	/**
     * 根据用户名密码，验证用户是否能成功登陆
     * @param string $user
     * @param string $pwd
     * @throws \Exception
     * @return mixed
     */
    public static function checkLogin($admin_data) {
            $where['admin_name'] = strip_tags(trim($admin_data["admin_name"]));
            $password = strip_tags(trim($admin_data["admin_pwd"]));
			$admin_info = AdminUser::where($where)->find()->toArray();
            
            if(!$admin_info){
                return ['code'=>0,'账户不存在'];
			}
            if($admin_info['status']==0){
                return ['code'=>0,'当前账户已被禁用'];
            }
            if(password($password)!=$admin_info['admin_pwd']){
                return ['code'=>0,'账户名或者密码错误'];
            }
            $group_id   = AuthGroupAccess::where('uid',$admin_info['id'])->value('group_id');
            $rules		= AuthGroup::where('id',$group_id)->value("rules");
            $admin_info['rules'] = $rules;
            unset($admin_info['admin_pwd']);
            if($admin_data["remember"]){
                Session::set('admin_info', $admin_info,7*24*3600);
                Session::set('admin_sign',  SignHelper::authSign($admin_info),7*24*3600);
            }else{
                Session::set('admin_info', $admin_info);
                Session::set('admin_sign',  SignHelper::authSign($admin_info));
            }

        return true;
		
	}
	
}
?>