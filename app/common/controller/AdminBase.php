<?php
/*
 * @Descripttion: 后台公共控制器
 * @version: 
 * @Author: cmg
 * @Date: 2020-01-09 11:41:59
 * @LastEditTime : 2020-01-09 14:44:45
 */
namespace app\common\controller;
use app\BaseController;
use think\facade\Session;
use think\facade\Request;
use cmg\helper\SignHelper;
class AdminBase extends BaseController{
    //定义过滤控制器
    protected $com_arr=[];
    //定义过滤方法
    protected $com_active_arr=['admin/Login/index'];

    protected function initialize() {
        parent::initialize();
        $this->checkAuth();
        self::getMenu();
    }

    public function checkAuth()
    {
        //获取当前模块控制器方法
            $url = Request::module() . '/' . Request::controller() . '/' . Request::action();
            var_dump($url);
            die();

            $admin= Session::get('admin_info');
            $admin_sign= Session::get('admin_sign') == SignHelper::authSign($admin) ? $admin['id'] : 0;
            
            // 签名验证
            if (empty($admin) || $admin_sign) {
               $this->redirect('/admin/login/idnex');
            }
        
        //针对角色进行权限设置
    }


    public static function getMenu(){
        
    }


}