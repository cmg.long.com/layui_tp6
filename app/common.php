<?php
// 应用公共文件
/**
 * 调用成功 code==200
 */
function toSuccess($msg='', $data='',$count=0,$pages=0,$code='200') {
    return json(['code'=>$code,'data'=>$data,'msg'=>$msg,'count'=>$count,'pages'=>$pages]);
}
/**
 * 调用失败
 */
function toError($code='', $msg='', $data='') {
    return json(['code'=>$code,'msg'=>$msg,'data'=>$data]);
}

/**
 * 管理员密码加密方式
 * @param $password  密码
 * @param $type 选择加密参数
 * @return string
 */
function password($password)
{
    $salt=config('app.salt');  //每个模块可以单独config设置盐值
    return md5(md5($password.$salt));
}